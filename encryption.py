from plumbum import SshMachine
from contextlib import contextmanager

from config import HOSTNAME, SSH_USER_NAME, IDENTITY_FILE, CONTAINER_PATH, CONTAINER, MOUNT_POINT, LUKS_PASSWORD

class RemoteLuksContainer:
    def __init__(self, hostname, user, identity_file, container_path, container_name, mount_point, luks_password):

        self.remote = SshMachine(hostname, user, keyfile=identity_file)
        self.user = user
        self.hostname = hostname
        self.container_path = container_path
        self.container_name = container_name
        self.mount_point = mount_point
        self.luks_password = luks_password

    def decrypt(self):
        # Mount Loop-Device

        # cryptsetup luksOpen /storage/my_container my_mount
        luks_open = self.remote['cryptsetup']['luksOpen'][self.container_path][self.container_name]
        echo_pass = self.remote['echo'][self.luks_password]
        self.__exec(echo_pass | luks_open)

    def mount(self):
        # mount /dev/mapper/ARCHIVE /media/ARCHIVE
        container_device = "/dev/mapper/{container_name}".format(container_name=self.container_name)
        mount_container = self.remote['mount'][container_device][self.mount_point]
        self.__exec(mount_container)

    def encrypt(self):
        #  cryptsetup luksClose ARCHIVE
        luks_close = self.remote['cryptsetup']['luksClose'][self.container_name]
        self.__exec(luks_close)

    def close(self):
        self.remote.close()

    def umount(self):
        umount = self.remote['umount']['-l'][self.mount_point]
        self.__exec(umount)

    def __exec(self, cmd):
        p = cmd.popen()
        out, err = p.communicate()

        if out:
            print(out)
        if err:
            print(err)


def build_encrypted_container():
    container = RemoteLuksContainer(
        HOSTNAME,
        SSH_USER_NAME,
        IDENTITY_FILE,
        CONTAINER_PATH,
        CONTAINER,
        MOUNT_POINT,
        LUKS_PASSWORD
    )
    
    return container

@contextmanager
def encrypted_container(*args, **kwargs):
    
    container = build_encrypted_container() 
     
    try:
        container.decrypt()
        container.mount()
        yield container
    finally:
        container.umount()
        container.encrypt()
        container.close()

