from encryption import encrypted_container
from sync import sync

from config import *


def main():
    """"""

    with encrypted_container() as remote:

        source = BACKUP_DIR
        dest = "{user}@{hostname}:{mount_point}".format(
            user=remote.user,
            hostname=remote.hostname,
            mount_point=remote.mount_point
        )

        sync(source, dest)


if __name__ == '__main__':
    main()
