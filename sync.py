from plumbum import local


def sync(source, dest):
    rsync = local['rsync']['-az']['-e ssh']['--delete']['--log-file=./rsync-status.txt'][source][dest]
    print(rsync())
